# Furmote
This is a small and simple Android app encoding all Furby commands into IR pulses which the classic 1998 Furbys use to communicate with each other. This way, you can make your Furbys respond in different ways, make them sick, or send them to sleep.

<img src="https://gitlab.com/Syroot/Furmote/raw/master/doc/readme/screenshot_1_framed.png" alt="Furmote Commands" width="300px"> <img src="https://gitlab.com/Syroot/Furmote/raw/master/doc/readme/screenshot_2_framed.png" alt="Furmote Commands" width="300px">

## Availability

- The APK files are available for manual installation on the [Tags](https://gitlab.com/Syroot/Furmote/tags) page.
