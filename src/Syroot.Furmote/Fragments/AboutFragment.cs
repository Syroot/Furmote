﻿using System;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;
using Android.Widget;
using Uri = Android.Net.Uri;

namespace Syroot.Furmote.Fragments
{
    /// <summary>
    /// Represents a page showing information about the app.
    /// </summary>
    internal class AboutFragment : FragmentBase
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.fragment_about, null);
            
            PackageInfo packageInfo = Context.PackageManager.GetPackageInfo(Context.PackageName, 0);
            TextView txtAboutVersion = view.FindViewById<TextView>(Resource.Id.lblAboutTitle);
            txtAboutVersion.Text = $"{Context.Resources.GetString(Resource.String.app_name)} {packageInfo.VersionName}";

            Button btnGitHub = view.FindViewById<Button>(Resource.Id.btnGitHub);
            btnGitHub.Click += BtnGitHub_Click;

            return view;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// The ID of the title to display in the action bar when the fragment is active.
        /// </summary>
        protected override int TitleID
        {
            get { return Resource.String.title_about; }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static AboutFragment CreateInstance()
        {
            return new AboutFragment { Arguments = new Bundle() };
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void OpenWebsite(string url)
        {
            Intent intent = new Intent(Intent.ActionView, Uri.Parse(url));
            StartActivity(intent);
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void BtnGitHub_Click(object sender, EventArgs e)
        {
            OpenWebsite("https://github.com/Syroot/Furmote");
        }
    }
}