﻿using Android.OS;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;

namespace Syroot.Furmote.Fragments
{
    /// <summary>
    /// Represents a page displaying the possible commands which can be sent to a Furby.
    /// </summary>
    internal class CommandsFragment : FragmentBase
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private FurbyController _controller;
        private GridView _grdCommands;
        private FurbyCommandItemAdapter _commandAdapter;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// The ID of the title to display in the action bar when the fragment is active.
        /// </summary>
        protected override int TitleID
        {
            get { return Resource.String.title_commands; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set up the furby controller.
            _controller = new FurbyController(Activity);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.fragment_commands, null);
            _grdCommands = view.FindViewById<GridView>(Resource.Id.grdCommands);
            
            // Set up gridview.
            _commandAdapter = new FurbyCommandItemAdapter(Activity);
            GridLayoutAnimationController controller = new GridLayoutAnimationController(
                AnimationUtils.LoadAnimation(Context, Resource.Animation.slide_in_bottom), 0.05f, 0.1f);
            _grdCommands.Adapter = _commandAdapter;
            _grdCommands.ItemClick += _lstCommands_ItemClick;
            _grdCommands.LayoutAnimation = controller;

            return view;
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static CommandsFragment CreateInstance()
        {
            return new CommandsFragment { Arguments = new Bundle() };
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void _lstCommands_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            FurbyCommandItem item = _commandAdapter[e.Position];
            _controller.Send(item.Command);
            Toast.MakeText(Context, $"{Resources.GetString(Resource.String.sent)} {item.Description}",
                ToastLength.Short).Show();
        }
    }
}