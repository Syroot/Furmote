﻿using Android.Support.V7.App;
using Fragment = Android.Support.V4.App.Fragment;

namespace Syroot.Furmote.Fragments
{
    /// <summary>
    /// Represents the common base for fragments displayed in the app.
    /// </summary>
    internal abstract class FragmentBase : Fragment
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// The ID of the title to display in the action bar when the fragment is active.
        /// </summary>
        protected abstract int TitleID { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void OnResume()
        {
            base.OnResume();
            AppCompatActivity activity = (AppCompatActivity)Activity;
            activity.SupportActionBar.SetTitle(TitleID);
        }
    }
}