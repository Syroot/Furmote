﻿using Android.OS;
using Android.Views;

namespace Syroot.Furmote.Fragments
{
    /// <summary>
    /// Represents a page showing instructions.
    /// </summary>
    internal class HelpFragment : FragmentBase
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            View view = inflater.Inflate(Resource.Layout.fragment_help, null);
            return view;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// The ID of the title to display in the action bar when the fragment is active.
        /// </summary>
        protected override int TitleID
        {
            get { return Resource.String.title_help; }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static HelpFragment CreateInstance()
        {
            return new HelpFragment { Arguments = new Bundle() };
        }        
    }
}