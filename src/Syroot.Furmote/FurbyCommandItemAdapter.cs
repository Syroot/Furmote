﻿using Android.App;
using Android.Views;
using Android.Widget;

namespace Syroot.Furmote
{
    /// <summary>
    /// Represents the <see cref="ListView"/> item adapter displaying the possible furby commands.
    /// </summary>
    internal class FurbyCommandItemAdapter : BaseAdapter<FurbyCommandItem>
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _commandCount = 16;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Activity _activity;
        private readonly FurbyCommandItem[] _items;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FurbyCommandItemAdapter"/> class for the given <paramref name="activity"/>.
        /// </summary>
        /// <param name="activity">The <see cref="Activity"/> to use.</param>
        public FurbyCommandItemAdapter(Activity activity)
        {
            _activity = activity;

            // Add the 15 possible commands.
            _items = new FurbyCommandItem[_commandCount];
            string[] descriptions = _activity.Resources.GetStringArray(Resource.Array.furby_command_descriptions);
            for (int i = 0; i < _commandCount; i++)
            {
                _items[i] = new FurbyCommandItem
                {
                    Command = (FurbyCommand)i,
                    Description = descriptions[i]
                };
            }
        }

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="FurbyCommandItem"/> at the given <paramref name="position"/>.
        /// </summary>
        /// <param name="position">The index of the <see cref="FurbyCommandItem"/> to retrieve.</param>
        /// <returns>The retrieved <see cref="FurbyCommandItem"/>.</returns>
        public override FurbyCommandItem this[int position] => _items[position];

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the number of items in the list.
        /// </summary>
        public override int Count => _commandCount;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the item at the given <see cref="position"/>. Not implemented.
        /// </summary>
        /// <param name="position">The index of the item to retrieve.</param>
        /// <returns>The retrieved item.</returns>
        public override Java.Lang.Object GetItem(int position) => null;

        /// <summary>
        /// Gets a unique identifier for the item at the given <paramref name="position"/>.
        /// </summary>
        /// <param name="position">The index of the item for which the identifier will be retrieved.</param>
        /// <returns>The unique identifier of the item.</returns>
        public override long GetItemId(int position) => (long)_items[position].Command;

        /// <summary>
        /// Gets a view representing the item at the given <paramref name="position"/>.
        /// </summary>
        /// <param name="position">The index of the item for which a view will be retrieved.</param>
        /// <param name="convertView">A <see cref="View"/> which will be reused to display this item.</param>
        /// <returns>A <see cref="View"/> displaying the item.</returns>
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            FurbyCommandItem item = _items[position];

            Button view = (Button)(convertView
                ?? _activity.LayoutInflater.Inflate(Resource.Layout.furby_command_item, parent, false));
            view.Text = item.Description;

            return view;
        }
    }

    internal class FurbyCommandItem
    {
        internal FurbyCommand Command;
        internal string Description;
    }
}