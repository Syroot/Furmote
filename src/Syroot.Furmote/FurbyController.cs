﻿using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.Hardware;
using Android.Runtime;

namespace Syroot.Furmote
{
    /// <summary>
    /// Represents an IR transmitter which generate pulses from 4-bit classic Furby (1998) commands.
    /// </summary>
    internal class FurbyController
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _pulseMicroseconds = 200;
        private const int _halfBitMicroseconds = 1000;
        private const int _packetMilliseconds = 100;
        private const int _commandRepeat = 4;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Activity _activity;
        private ConsumerIrManager _irManager;
        private readonly int _frequency;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal FurbyController(Activity activity)
        {
            _activity = activity;
            _irManager = _activity.GetSystemService(Context.ConsumerIrService).JavaCast<ConsumerIrManager>();
            if (_irManager.HasIrEmitter)
                _frequency = _irManager.GetCarrierFrequencies()[0].MinFrequency;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets a value indicating whether Furby commands can be sent.
        /// </summary>
        internal bool CanSend => _irManager.HasIrEmitter;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Sends the given <paramref name="command"/>.
        /// </summary>
        /// <param name="command">The <see cref="FurbyCommand"/> to send.</param>
        internal async void Send(FurbyCommand command)
        {
            // Transmit the data twice for a complete command.
            int[] pulses = GetPulses(command);
            for (int i = 0; i < _commandRepeat; i++)
                await _irManager.TransmitAsync(_frequency, pulses);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private int[] GetPulses(FurbyCommand command)
        {
            // Compute the 9-bit commands.
            byte value = (byte)command;
            bool[] bits = new bool[9];
            bits[0] = true; // Initial
            for (int i = 0; i < 4; i++)
            {
                bool on = (value & 1 << i) != 0;
                bits[i + 1] = on; // Command positive
                bits[i + 5] = !on; // Command negative
            }

            // Build the pulses from the bits.
            List<int> pulses = new List<int>();
            foreach (bool bit in bits)
            {
                if (bit)
                {
                    pulses.Add(_pulseMicroseconds); // on
                    pulses.Add(_halfBitMicroseconds - _pulseMicroseconds); // off
                    pulses.Add(_pulseMicroseconds); // on
                    pulses.Add(_halfBitMicroseconds - _pulseMicroseconds); // off
                }
                else
                {
                    pulses.Add(_pulseMicroseconds); // on
                    pulses.Add(_halfBitMicroseconds * 2 - _pulseMicroseconds); // off
                }
            }
            // Stretch last off signal so that the packet spans 100ms.
            pulses[pulses.Count - 1] += (_packetMilliseconds * 1000) - (9/*bits*/ * _halfBitMicroseconds * 2);

            return pulses.ToArray();
        }
    }

    /// <summary>
    /// Represents the possible commands which can be sent to a Furby.
    /// </summary>
    public enum FurbyCommand : byte
    {
        Hello1,
        Hello2,
        Hello3,
        Hello4,
        Hello5,
        Party,
        Dance1,
        Dance2,
        Sneeze,
        SingBurp,
        LaughTickle,
        SingTwinkle,
        YawnHide,
        Hide,
        FeelSleepy,
        Sleep
    }
}
