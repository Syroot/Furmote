﻿using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Views;
using Syroot.Furmote.Fragments;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace Syroot.Furmote
{
    /// <summary>
    /// Represents the main view of the app allowing to navigate between fragments with a menu drawer.
    /// </summary>
    [Activity(Label = "Furmote", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Toolbar _toolbar;
        private DrawerLayout _drawerLayout;
        private NavigationView _navigationView;
        private IMenuItem _currentMenuItem;
        private int _currentFrameID;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            // Open the navigation drawer.
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    _drawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Do not continue creating the app if the FurbyController cannot work, making the app useless.
            FurbyController controller = new FurbyController(this);
            if (!controller.CanSend)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .SetTitle(Resources.GetString(Resource.String.ir_not_found_title))
                    .SetMessage(Resources.GetString(Resource.String.ir_not_found_message))
                    .SetPositiveButton(Resources.GetString(Resource.String.close_app), (s, e) =>
                    {
                        Finish();
                    })
                    .SetCancelable(false);
                builder.Show();
                return;
            }

            SetContentView(Resource.Layout.main);

            // Set up the toolbar.
            _toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(_toolbar);
            SupportActionBar.Title = Resources.GetString(Resource.String.app_name);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_action_menu);

            // Set up the navigation.
            _drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            _navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
            _navigationView.NavigationItemSelected += _navigationView_NavigationItemSelected;

            // Open the first page at startup. Restore the title on orientation change.
            if (savedInstanceState == null)
                Navigate(Resource.Id.nav_commands);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Navigate(int frameID)
        {
            // Do not navigate if the requested frame is the same as the current one.
            if (frameID != _currentFrameID)
            {
                _currentFrameID = frameID;

                // Check only the active item.
                _currentMenuItem?.SetChecked(false);
                _navigationView.SetCheckedItem(_currentFrameID);

                // Retrieve the new fragment and replace the content with the new fragment.
                FragmentBase fragment = null;
                switch (_currentFrameID)
                {
                    case Resource.Id.nav_commands:
                        fragment = CommandsFragment.CreateInstance();
                        break;
                    case Resource.Id.nav_help:
                        fragment = HelpFragment.CreateInstance();
                        break;
                    case Resource.Id.nav_about:
                        fragment = AboutFragment.CreateInstance();
                        break;
                }
                SupportFragmentManager.BeginTransaction()
                    .Replace(Resource.Id.content_frame, fragment)
                    .Commit();
            }
        }
        
        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void _navigationView_NavigationItemSelected(object sender,
            NavigationView.NavigationItemSelectedEventArgs e)
        {
            Navigate(e.MenuItem.ItemId);
            _currentMenuItem = e.MenuItem;
            _drawerLayout.CloseDrawers();
        }
    }
}

