﻿using System.Reflection;
using System.Runtime.InteropServices;
using Android.App;

[assembly: AssemblyTitle("Furmote")]
[assembly: AssemblyDescription("Furby Remote Controlling")]
[assembly: AssemblyCompany("(c) Syroot")]
[assembly: AssemblyProduct("Syroot.Furmote")]
[assembly: AssemblyCopyright("MIT")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: ComVisible(false)]

#if DEBUG
[assembly: Application(Debuggable = true)]
#else
[assembly: Application(Debuggable = false)]
#endif